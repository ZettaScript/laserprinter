pub struct PrinterConfig {
	pub r_speed: u8,
	/// S motor duration to release entire sheet (ms)
	pub s_duration: u16,
	/// Wait this delay to ensure the printer is ready (ms)
	pub startup_delay: u64,
	/// Printable zone stop (0 is origin)
	pub x_max: i16,
	/// Printable zone start (0 is origin)
	pub x_min: i16,
	/// Delay before going back in the opposite direction (to avoid too much mechanical stress) (ms)
	pub x_reverse_delay: u16,
	pub x_speed: u8,
	/// Delay after moving, to ensure there is no movement (ms)
	pub x_stabilize_delay: u16,
	/// X step length (mm)
	pub x_step_length: f32,
}

impl Default for PrinterConfig {
	fn default() -> Self {
		Self {
			r_speed: 253,
			s_duration: 1000,
			startup_delay: 2000,
			x_max: 1000,
			x_min: 300,
			x_reverse_delay: 50,
			x_speed: 240,
			x_stabilize_delay: 75,
			x_step_length: 0.5,
		}
	}
}
