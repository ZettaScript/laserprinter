mod calibrate;
mod cli;
mod print_image;
mod printer_config;

use common::protocol::*;

use clap::Parser;
use hubpack::SerializedSize;
use std::{
	io::{self, Read, Write},
	thread::sleep,
	time::Duration,
};

#[derive(Debug)]
enum Error {
	BadChecksum,
	BadResponse,
	Serial(io::Error),
}

fn main() {
	let opt = cli::MainOpt::parse();

	match opt.subcommand {
		cli::Subcommand::Calibrate => calibrate::calibrate(),
		cli::Subcommand::PrintImage(subopt) => print_image::print_image_file(&subopt),
		cli::Subcommand::ReleaseSheet(subopt) => release_sheet(&subopt),
		cli::Subcommand::Test => test_printer(),
	}
}

fn init_serial() -> Box<dyn serialport::SerialPort> {
	let ports = serialport::available_ports().expect("Cannot list available ports");

	println!("Available ports:");
	for port in ports.iter() {
		println!("{port:?}");
	}
	println!();

	let serial = serialport::new(&ports.get(0).expect("No available port").port_name, 57600)
		.timeout(std::time::Duration::from_secs(10))
		.open()
		.expect("Cannot open port");

	serial
}

fn send_commands<S: Write>(serial: &mut S, cmds: &[Command]) {
	let mut buffer = [0; 257];
	let mut len = 0;
	for cmd in cmds {
		len += hubpack::serialize(&mut buffer[2 + len..2 + len + Command::MAX_SIZE], cmd).unwrap();
	}
	if len > 255 {
		panic!("Packet length is {len} while maximum is 255.");
	}
	buffer[0] = len as u8;
	buffer[1] = compute_checksum(&buffer[2..2 + len]);

	serial.write_all(&buffer[0..2 + len]).unwrap();
}

fn wait_for_response<R: Read>(serial: &mut R) -> Result<Vec<Response>, Error> {
	let mut buffer = [0; 257];
	let mut len = serial.read(&mut buffer).map_err(Error::Serial)?;
	let resp_len = buffer[0] as usize;
	while len < resp_len + 2 {
		len += serial
			.read(&mut buffer[len..resp_len + 2])
			.map_err(Error::Serial)?;
	}
	let mut raw_resps = &buffer[2..resp_len + 2];

	if buffer[1] != compute_checksum(raw_resps as &_) {
		println!("Recv bad checksum: {buffer:?}");
		return Err(Error::BadChecksum);
	}

	let mut resps = Vec::new();
	while !raw_resps.is_empty() {
		if let Ok((resp, rest_resps)) = hubpack::deserialize::<Response>(raw_resps) {
			raw_resps = rest_resps;
			println!("Recv: {resp:?}");
			resps.push(resp);
		} else {
			println!("Recv bad response after these: {resps:?}");
			return Err(Error::BadResponse);
		}
	}
	Ok(resps)
}

fn release_sheet(subopt: &cli::ReleaseSheetSubcommand) {
	let mut serial = init_serial();

	sleep(Duration::from_millis(2000));
	send_commands(&mut serial, &[Command::RMove { r: subopt.r }]);
}

fn test_printer() {
	let mut serial = init_serial();

	sleep(Duration::from_millis(2000));

	// Test r motor
	/*for r_speed in [180, 190, 200, 210, 220, 230, 240, 250, 255] {
		send_commands(
			&mut serial,
			&[
				Command::RSpeedSet { r_speed },
				Command::RMove { r: 100 },
				Command::TimeGet,
			],
		);
		wait_for_response(&mut serial).unwrap();
	}*/

	// Test s motor
	/*for s_speed in [180, 190, 200, 210, 220, 230, 240, 250, 255] {
		send_commands(&mut serial,&[
			Command::SSpeedSet{s_speed},
			Command::SMove{s: 100},
			Command::TimeGet,
		]
		);
		wait_for_response(&mut serial).unwrap();
	}*/

	// Test x motor & x encoder
	/*for x_speed in [180, 190, 200, 210, 220, 230, 240, 250] {
		send_commands(
			&mut serial,
			&[
				Command::XSpeedSet { x_speed },
				Command::XMoveToOrigin,
				Command::XMoveRelative { x: 100 },
				Command::XGet,
			],
		);
		wait_for_response(&mut serial).unwrap();
	}*/

	// Test r motor regularity
	/*send_commands(
		&mut serial,
		&[
			Command::RSpeedSet { r_speed: 210 },
			Command::XSpeedSet { x_speed: 220 },
			Command::XMoveToOrigin,
			Command::XMoveRelative { x: 350 },
			Command::XGet,
		],
	);
	wait_for_response(&mut serial).unwrap();
	for _ in 0..30 {
		send_commands(
			&mut serial,
			&[
				Command::LaserSet { power: 200 },
				Command::Wait { duration: 80 },
				Command::LaserSet { power: 0 },
				Command::RMove { r: 15 },
				Command::Wait { duration: 50 },
				Command::Ping,
			],
		);
		wait_for_response(&mut serial).unwrap();
	}
	send_commands(&mut serial, &[Command::XMoveToOrigin]);*/

	// X motor
	send_commands(
		&mut serial,
		&[
			Command::XSpeedSet { x_speed: 255 },
			//Command::XMoveToOrigin,
			//Command::Wait { duration: 50 },
			Command::XMoveRelative { x: -50 },
			Command::Ping,
		],
	);
	wait_for_response(&mut serial).unwrap();

	// Laser
	/*send_commands(
		&mut serial,
		&[
			Command::LaserSet { power: 240 },
			Command::Wait { duration: 50 },
			Command::LaserSet { power: 0 },
			Command::Ping,
		]
	);
	wait_for_response(&mut serial).unwrap();*/

	// Line buffer
	/*send_commands(
		&mut serial,
		&[
			Command::RSpeedSet { r_speed: 253 },
			Command::XSpeedSet { x_speed: 220 },
			Command::XMoveToOrigin,
			Command::XSpeedSet { x_speed: 155 },
			Command::Wait { duration: 50 },
			Command::XSet { x: 0 },
			Command::XMoveRelative { x: 300 },
			Command::LineBufferSet {
				offset: 0,
				data: (0u8..32)
					.map(|i| (i % 2)*255)
					.collect::<Vec<u8>>()
					.try_into()
					.unwrap(),
			},
			Command::LineBufferPrint {
				offset: 0,
				len: 32,
				x: 600,
			},
			Command::RMove { r: 12 },
			Command::Ping,
		],
	);
	wait_for_response(&mut serial).unwrap();*/

	// Monitor
	/*loop {
		send_commands(&mut serial, &[Command::TimeGet, Command::XGet]);
		wait_for_response(&mut serial).unwrap();
		wait_for_response(&mut serial).unwrap();
		std::thread::sleep(Duration::from_millis(500));
	}*/
}
