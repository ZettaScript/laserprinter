use crate::{cli, printer_config::PrinterConfig};

use common::protocol::*;
use std::{thread::sleep, time::Duration};

pub fn print_image_file(subopt: &cli::PrintImageSubcommand) {
	let printer_config = PrinterConfig::default();

	let img = image::open(&subopt.file)
		.expect("Cannot open image file")
		.grayscale();

	let mut img = img.to_luma_alpha8();
	img.pixels_mut().for_each(|pixel| {
		*pixel = image::LumaA([
			((255 - pixel.0[0]) as u16 * pixel.0[1] as u16 / 255) as u8,
			255,
		])
	});

	print_image(&img, &printer_config);
}

fn print_image(
	image: &image::ImageBuffer<image::LumaA<u8>, Vec<u8>>,
	printer_config: &PrinterConfig,
) {
	let mut serial = crate::init_serial();

	sleep(Duration::from_millis(printer_config.startup_delay));

	crate::send_commands(
		&mut serial,
		&[
			Command::XSpeedSet {
				x_speed: printer_config.x_speed,
			},
			Command::RSpeedSet {
				r_speed: printer_config.r_speed,
			},
			Command::XGet,
		],
	);
	crate::wait_for_response(&mut serial).unwrap();

	for row in image.rows() {
		crate::send_commands(
			&mut serial,
			&[
				Command::XMoveToOrigin,
				Command::Wait {
					duration: printer_config.x_reverse_delay,
				},
				Command::XSet { x: 0 },
				Command::XMoveAbsolute {
					x: printer_config.x_min,
				},
				Command::XGet,
			],
		);
		crate::wait_for_response(&mut serial).unwrap();

		let mut x = 0;

		for image::LumaA([pixel_value, _]) in row {
			x += 1;

			if *pixel_value > 127 {
				crate::send_commands(
					&mut serial,
					&[
						Command::XMoveRelative { x },
						Command::Wait {
							duration: printer_config.x_stabilize_delay,
						},
						Command::LaserSet { power: 255 },
						Command::Wait { duration: 50 },
						Command::LaserSet { power: 0 },
						Command::XGet,
					],
				);
				crate::wait_for_response(&mut serial).unwrap();
				x = 0
			}
		}

		crate::send_commands(&mut serial, &[Command::RMove { r: 10 }, Command::XGet]);
		crate::wait_for_response(&mut serial).unwrap();
	}
	crate::send_commands(&mut serial, &[Command::XMoveToOrigin]);
}
