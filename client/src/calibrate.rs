use common::protocol::*;
use std::{thread::sleep, time::Duration};

pub fn calibrate() {
	let mut serial = crate::init_serial();

	sleep(Duration::from_millis(2000));

	// Prepare
	crate::send_commands(
		&mut serial,
		&[
			Command::XSpeedSet { x_speed: 220 },
			Command::RSpeedSet { r_speed: 240 },
			Command::XGet,
		],
	);
	crate::wait_for_response(&mut serial).unwrap();

	// Test r parameters
	crate::send_commands(
		&mut serial,
		&[
			Command::XMoveToOrigin,
			Command::XSet { x: 0 },
			Command::XMoveAbsolute { x: 600 },
			Command::XGet,
		],
	);
	crate::wait_for_response(&mut serial).unwrap();
	for r_duration in [20, 18, 16, 14, 12, 10, 8] {
		for r_speed in [255, 250, 245, 240, 235, 230, 225, 220, 215, 210] {
			crate::send_commands(
				&mut serial,
				&[
					Command::RSpeedSet { r_speed },
					Command::RMove { r: r_duration },
					Command::Wait { duration: 50 },
					Command::LaserSet { power: 200 },
					Command::Wait { duration: 90 },
					Command::LaserSet { power: 0 },
					Command::Ping,
				],
			);
			crate::wait_for_response(&mut serial).unwrap();
		}
	}
}
