use clap::Parser;

#[derive(Clone, Debug, Parser)]
#[clap(name = "client")]
pub struct MainOpt {
	#[clap(subcommand)]
	pub subcommand: Subcommand,
}

#[derive(Clone, Debug, clap::Subcommand)]
pub enum Subcommand {
	Calibrate,
	PrintImage(PrintImageSubcommand),
	ReleaseSheet(ReleaseSheetSubcommand),
	Test,
}

#[derive(Clone, Debug, Parser)]
pub struct PrintImageSubcommand {
	pub file: String,
}

#[derive(Clone, Debug, Parser)]
pub struct ReleaseSheetSubcommand {
	#[clap(default_value_t = 1000)]
	pub r: u16,
}
