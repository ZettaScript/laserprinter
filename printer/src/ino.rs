#![allow(dead_code)]

use arduino_hal::simple_pwm::*;
use atmega_hal::port::*;

pub use pins::*;

// Pins
#[cfg(feature = "board-mega2560")]
mod pins {
	use super::*;

	pub type A0 = PC0;
	pub type A1 = PC1;
	pub type A2 = PC2;
	pub type A3 = PC3;
	pub type A4 = PC4;
	pub type A5 = PC5;
	pub type D0 = PE0;
	pub type D1 = PE1;
	pub type D2 = PE4;
	pub type D3 = PE5;
	pub type D4 = PG5;
	pub type D5 = PE3;
	pub type D6 = PH3;
	pub type D7 = PH4;
	pub type D8 = PH5;
	pub type D9 = PH6;
	pub type D10 = PB4;
	pub type D11 = PB5;
	pub type D12 = PB6;
	pub type D13 = PB7;
	pub type D14 = PJ1;
	pub type D15 = PJ0;
	pub type D16 = PH1;
	pub type D17 = PH0;
	pub type D18 = PD3;
	pub type D19 = PD2;
	pub type D20 = PD1;
	pub type D21 = PD0;
	pub type D22 = PA0;
	pub type D23 = PA1;
	pub type D24 = PA2;
	pub type D25 = PA3;
	pub type D26 = PA4;
	pub type D27 = PA5;
	pub type D28 = PA6;
	pub type D29 = PA7;
	pub type D30 = PC7;
	pub type D31 = PC6;
	pub type D32 = PC5;
	pub type D33 = PC4;
	pub type D34 = PC3;
	pub type D35 = PC2;
	pub type D36 = PC1;
	pub type D37 = PC0;
	pub type D38 = PD7;
	pub type D39 = PG2;
	pub type D40 = PG1;
	pub type D41 = PG0;
	pub type D42 = PL7;
	pub type D43 = PL6;
	pub type D44 = PL5;
	pub type D45 = PL4;
	pub type D46 = PL3;
	pub type D47 = PL2;
	pub type D48 = PL1;
	pub type D49 = PL0;
	pub type D50 = PB3;
	pub type D51 = PB2;
	pub type D52 = PB1;
	pub type D53 = PB0;

	pub type D5TimerPwm = Timer3Pwm;
	pub type D6TimerPwm = Timer4Pwm;
	pub type D9TimerPwm = Timer2Pwm;
	pub type D10TimerPwm = Timer2Pwm;
	pub type D11TimerPwm = Timer1Pwm;
}
#[cfg(feature = "board-uno")]
mod pins {
	use super::*;

	pub type A0 = PC0;
	pub type A1 = PC1;
	pub type A2 = PC2;
	pub type A3 = PC3;
	pub type A4 = PC4;
	pub type A5 = PC5;
	pub type D0 = PD0;
	pub type D1 = PD1;
	pub type D2 = PD2;
	pub type D3 = PD3;
	pub type D4 = PD4;
	pub type D5 = PD5;
	pub type D6 = PD6;
	pub type D7 = PD7;
	pub type D8 = PB0;
	pub type D9 = PB1;
	pub type D10 = PB2;
	pub type D11 = PB3;
	pub type D12 = PB4;
	pub type D13 = PB5;

	pub type D5TimerPwm = Timer0Pwm;
	pub type D6TimerPwm = Timer0Pwm;
	pub type D9TimerPwm = Timer1Pwm;
	pub type D10TimerPwm = Timer1Pwm;
	pub type D11TimerPwm = Timer2Pwm;
}

// Interrupts
pub const INT_LOW: u8 = 0;
pub const INT_HIGH: u8 = 1;
pub const INT_CHANGE: u8 = 2;
pub const INT_FALLING: u8 = 3;
pub const INT_RISING: u8 = 4;
