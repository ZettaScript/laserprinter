#![no_std]
#![no_main]
#![feature(abi_avr_interrupt)]

mod ino;
mod millis;

use common::protocol::*;
use ino::*;
use millis::*;

use arduino_hal::{
	port::{mode::*, Pin},
	prelude::*,
	simple_pwm::*,
};
//use avr_hal_generic::port::PinOps;
use core::{
	mem::MaybeUninit,
	sync::atomic::{compiler_fence /* , AtomicI16 */, Ordering::SeqCst},
};
use embedded_hal::serial::{Read, Write};
use hubpack::SerializedSize;
use panic_halt as _;

#[cfg(feature = "board-mega2560")]
const LINE_BUFFER_LEN: usize = 4096;
#[cfg(feature = "board-uno")]
const LINE_BUFFER_LEN: usize = 1024;
const X_TIMEOUT: u32 = 2_000;

//static mut X: AtomicI16 = AtomicI16::new(0);
//static mut PINS: mem::MaybeUninit<[port::Pin<port::mode::Output>; 10]> = mem::MaybeUninit::uninit();
static mut PRINTER: MaybeUninit<Printer> = MaybeUninit::uninit();

/*fn set_x(x: i16) {
	unsafe {
		X.store(x, SeqCst);
	}
}

fn get_x() -> i16 {
	unsafe { X }
}*/

#[inline(always)]
fn int_x_1() {
	let printer = unsafe { &mut *PRINTER.as_mut_ptr() };
	if printer.x_encoder_1.is_high() ^ printer.x_encoder_2.is_high() {
		printer.x = printer.x.wrapping_add(1);
	//printer.x.store(printer.x.load(SeqCst) + 1, SeqCst);
	} else {
		printer.x = printer.x.wrapping_sub(1);
		//printer.x.store(printer.x.load(SeqCst) - 1, SeqCst);
	}
	//printer.led.toggle();
}

#[inline(always)]
fn int_x_2() {
	let printer = unsafe { &mut *PRINTER.as_mut_ptr() };
	if printer.x_encoder_1.is_high() ^ printer.x_encoder_2.is_high() {
		printer.x = printer.x.wrapping_sub(1);
	//printer.x.store(printer.x.load(SeqCst) - 1, SeqCst);
	} else {
		printer.x = printer.x.wrapping_add(1);
		//printer.x.store(printer.x.load(SeqCst) + 1, SeqCst);
	}
	//printer.led.toggle();
}

#[cfg(feature = "board-uno")]
#[avr_device::interrupt(atmega328p)]
fn INT0() {
	int_x_1()
}

#[cfg(feature = "board-uno")]
#[avr_device::interrupt(atmega328p)]
fn INT1() {
	int_x_2()
}

#[cfg(feature = "board-mega2560")]
#[avr_device::interrupt(atmega2560)]
fn INT4() {
	int_x_1()
}

#[cfg(feature = "board-mega2560")]
#[avr_device::interrupt(atmega2560)]
fn INT5() {
	int_x_2()
}

struct Printer {
	busy_led: Pin<Output, D13>,
	error_led: Pin<Output, D12>,
	laser: Pin<PwmOutput<D9TimerPwm>, D9>,
	line_buffer: [u8; LINE_BUFFER_LEN],
	r_motor: Pin<PwmOutput<D6TimerPwm>, D6>,
	r_speed: u8,
	s_motor: Pin<PwmOutput<D5TimerPwm>, D5>,
	s_speed: u8,
	serial: arduino_hal::Usart<arduino_hal::pac::USART0, Pin<Input, D0>, Pin<Output, D1>>,
	x: i16, //AtomicI16,
	x_encoder_1: Pin<Input<Floating>, D3>,
	x_encoder_2: Pin<Input<Floating>, D2>,
	x_endstop: Pin<Input<PullUp>, D8>,
	x_motor_1: Pin<PwmOutput<D11TimerPwm>, D11>,
	x_motor_2: Pin<PwmOutput<D10TimerPwm>, D10>,
	x_speed: u8,
}

impl Printer {
	fn new() -> Self {
		let dp = arduino_hal::Peripherals::take().unwrap();
		let pins = arduino_hal::pins!(dp);

		millis_init(&dp.TC0);

		#[cfg(feature = "board-uno")]
		let timer0 = Timer0Pwm::new(dp.TC0, Prescaler::Prescale64);
		let timer1 = Timer1Pwm::new(dp.TC1, Prescaler::Prescale64);
		let timer2 = Timer2Pwm::new(dp.TC2, Prescaler::Prescale64);
		#[cfg(feature = "board-mega2560")]
		let timer3 = Timer3Pwm::new(dp.TC3, Prescaler::Prescale64);
		#[cfg(feature = "board-mega2560")]
		let timer4 = Timer4Pwm::new(dp.TC4, Prescaler::Prescale64);

		#[cfg(feature = "board-mega2560")]
		let mut laser = pins.d9.into_output().into_pwm(&timer2);
		#[cfg(feature = "board-uno")]
		let mut laser = pins.d9.into_output().into_pwm(&timer1);
		laser.enable();
		laser.set_duty(0);

		#[cfg(feature = "board-mega2560")]
		let mut x_motor_1 = pins.d11.into_output().into_pwm(&timer1);
		#[cfg(feature = "board-uno")]
		let mut x_motor_1 = pins.d11.into_output().into_pwm(&timer2);
		x_motor_1.enable();
		x_motor_1.set_duty(0);
		#[cfg(feature = "board-mega2560")]
		let mut x_motor_2 = pins.d10.into_output().into_pwm(&timer2);
		#[cfg(feature = "board-uno")]
		let mut x_motor_2 = pins.d10.into_output().into_pwm(&timer1);
		x_motor_2.enable();
		x_motor_2.set_duty(0);
		#[cfg(feature = "board-mega2560")]
		let mut r_motor = pins.d6.into_output().into_pwm(&timer4);
		#[cfg(feature = "board-uno")]
		let mut r_motor = pins.d6.into_output().into_pwm(&timer0);
		r_motor.enable();
		r_motor.set_duty(0);
		#[cfg(feature = "board-mega2560")]
		let mut s_motor = pins.d5.into_output().into_pwm(&timer3);
		#[cfg(feature = "board-uno")]
		let mut s_motor = pins.d5.into_output().into_pwm(&timer0);
		s_motor.enable();
		s_motor.set_duty(0);

		let x_encoder_1 = pins.d3.into_floating_input();
		let x_encoder_2 = pins.d2.into_floating_input();

		compiler_fence(SeqCst);

		// X encoder interrupts
		#[cfg(feature = "board-mega2560")]
		{
			dp.EXINT.eicrb.modify(|_, w| w.isc4().bits(INT_CHANGE));
			dp.EXINT.eicrb.modify(|_, w| w.isc5().bits(INT_CHANGE));
			dp.EXINT.eimsk.modify(|_, w| w.int().bits(1 << 4 | 1 << 5));
		}
		#[cfg(feature = "board-uno")]
		{
			dp.EXINT.eicra.modify(|_, w| w.isc0().bits(INT_CHANGE));
			dp.EXINT.eimsk.modify(|_, w| w.int0().set_bit());
			dp.EXINT.eicra.modify(|_, w| w.isc1().bits(INT_CHANGE));
			dp.EXINT.eimsk.modify(|_, w| w.int1().set_bit());
		}

		unsafe { avr_device::interrupt::enable() };

		compiler_fence(SeqCst);

		let serial = arduino_hal::default_serial!(dp, pins, 57600);

		Self {
			busy_led: pins.d13.into_output(),
			error_led: pins.d12.into_output(),
			laser,
			line_buffer: [0; LINE_BUFFER_LEN],
			r_motor,
			r_speed: 255,
			s_motor,
			s_speed: 255,
			serial,
			x: 0, //AtomicI16::new(0),
			x_encoder_1,
			x_encoder_2,
			x_endstop: pins.d8.into_pull_up_input(),
			x_motor_1,
			x_motor_2,
			x_speed: 180,
		}
	}

	fn read_cmds<'a>(&mut self, cmds_buffer: &'a mut [u8]) -> &'a [u8] {
		let len = nb::block!(self.serial.read()).void_unwrap();
		let checksum = nb::block!(self.serial.read()).void_unwrap();
		let cmds = &mut cmds_buffer[0..len as usize];

		// Read batch
		for b in cmds.iter_mut() {
			*b = nb::block!(self.serial.read()).void_unwrap();
		}

		if compute_checksum(cmds as &_) != checksum {
			self.error_led.set_high();
			self.send_response(Response::BadChecksum);
			return &[];
		}
		self.error_led.set_low();

		cmds as &[u8]
	}

	fn exec_cmds(&mut self, mut cmds: &[u8]) {
		self.busy_led.set_high();
		while !cmds.is_empty() {
			if let Ok((cmd, rest_cmds)) = hubpack::deserialize(cmds) {
				cmds = rest_cmds;
				self.exec_cmd(cmd);
			} else {
				break;
			}
		}
		self.busy_led.set_low();
	}

	fn exec_cmd(&mut self, cmd: Command) {
		match cmd {
			Command::LaserSet { power } => {
				self.laser.set_duty(power);
			}
			Command::LineBufferLenGet => self.send_response(Response::LineBufferLen {
				len: LINE_BUFFER_LEN as _,
			}),
			Command::LineBufferSet { offset, data } => {
				let offset = offset as usize;
				if let Some(buffer) = self
					.line_buffer
					.get_mut(offset..offset.wrapping_add(data.len()))
				{
					buffer.copy_from_slice(&data);
				}
			}
			Command::LineBufferPrint { offset, len, x } => {
				self.line_buffer_print(offset as usize, len as usize, x)
			}
			Command::Ping => self.send_response(Response::Pong),
			Command::RMove { r } => self.move_r(r),
			Command::RSpeedSet { r_speed } => self.r_speed = r_speed,
			Command::SMove { s } => self.move_s(s),
			Command::SSpeedSet { s_speed } => self.s_speed = s_speed,
			Command::TimeGet => self.send_response(Response::Time { time: millis() }),
			Command::Wait { duration } => arduino_hal::delay_ms(duration),
			Command::XGet => self.send_response(Response::X {
				x: self.x, //self.x.load(SeqCst),
			}),
			Command::XMoveAbsolute { x } => self.move_x_absolute(x),
			Command::XMoveRelative { x } => self.move_x_relative(x),
			Command::XMoveToOrigin => self.move_x_origin(),
			Command::XSet { x } => self.x = x, //self.x.store(x, SeqCst),
			Command::XSpeedSet { x_speed } => self.x_speed = x_speed,
			//_ => self.send_response(Response::UnknownCommand),
		}
	}

	fn send_response(&mut self, resp: Response) {
		let mut buffer = [0; Response::MAX_SIZE];
		let resp_len = hubpack::serialize(&mut buffer, &resp).unwrap();
		let checksum = compute_checksum(&buffer[0..resp_len]);
		self.serial.write(resp_len as u8).ok();
		arduino_hal::delay_us(500);
		self.serial.write(checksum).ok();
		for b in &buffer[0..resp_len] {
			arduino_hal::delay_us(500);
			self.serial.write(*b).ok();
		}
	}

	fn move_r(&mut self, r: u16) {
		self.r_motor.set_duty(self.r_speed);
		arduino_hal::delay_ms(r);
		self.r_motor.set_duty(0);
	}

	fn move_s(&mut self, s: u16) {
		self.s_motor.set_duty(self.s_speed);
		arduino_hal::delay_ms(s);
		self.s_motor.set_duty(0);
	}

	fn move_x_relative(&mut self, x: i16) {
		self.move_x_absolute(self.x.saturating_add(x));
	}

	fn move_x_absolute(&mut self, target_x: i16) {
		let timeout = millis() + X_TIMEOUT;
		if target_x > self.x {
			self.x_motor_2.set_duty(self.x_speed);
			while self.x < target_x && millis() < timeout {}
			self.x_motor_2.set_duty(0);
		} else {
			self.x_motor_1.set_duty(self.x_speed);
			while self.x > target_x && millis() < timeout {}
			self.x_motor_1.set_duty(0);
		}
	}

	fn move_x_origin(&mut self) {
		let timeout = millis() + X_TIMEOUT;
		self.x_motor_1.set_duty(self.x_speed);
		while self.x_endstop.is_high() && millis() < timeout {}
		self.x_motor_1.set_duty(0);
	}

	fn line_buffer_print(&mut self, offset: usize, len: usize, x: i16) {
		if offset.saturating_add(len) >= LINE_BUFFER_LEN || x == 0 {
			return;
		}

		let x_from = self.x;
		let x_to = x_from + x;
		let f = len as f32 / x as f32;

		let timeout = millis() + X_TIMEOUT;
		if x > 0 {
			self.x_motor_2.set_duty(self.x_speed);
			while self.x < x_to && millis() < timeout {
				let ind = ((self.x - x_from) as f32 * f) as usize;
				if ind < len {
					self.laser.set_duty(self.line_buffer[offset + ind]);
				}
			}
			self.x_motor_2.set_duty(0);
		} else {
			self.x_motor_1.set_duty(self.x_speed);
			while self.x > x_to && millis() < timeout {
				let ind = ((self.x - x_from) as f32 * f) as usize;
				if ind < len {
					self.laser.set_duty(self.line_buffer[offset + ind]);
				}
			}
			self.x_motor_1.set_duty(0);
		}
		self.laser.set_duty(0);
	}
}

#[arduino_hal::entry]
fn main() -> ! {
	let printer = unsafe {
		PRINTER = MaybeUninit::new(Printer::new());
		compiler_fence(SeqCst);
		&mut *PRINTER.as_mut_ptr()
	};

	let mut cmds_buffer = [0; 255];
	loop {
		let cmds = printer.read_cmds(&mut cmds_buffer);
		printer.exec_cmds(cmds);
	}
}
