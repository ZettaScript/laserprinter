module BeltDriveSupport(
	drive_d = 5.1,
	drive_h = 16,
	rod_d = 3.5,
) {
	difference() {
		cylinder(h=drive_h, d=drive_d, $fn=60);
		cylinder(h=drive_h, d=rod_d, $fn=60);
	}
}

module GearSupport(
	d = 6,
	gear_d = 21,
	gear_h = 2.5,
	gear_hole_d = 3.5,
	gear_hole_dist = 6.5,
	gear_hole_n = 4,
	h = 8,
	rod_d = 3.5,
) {
	difference() {
		union() {
			cylinder(h=h, d=d, $fn=60);
			cylinder(h=gear_h, d=gear_d, $fn=100);
		}
		
		cylinder(h=h, d=rod_d, $fn=60);
		for(i = [0 : gear_hole_n-1]) {
			rotate([0, 0, 90*i]) translate([gear_hole_dist, 0, 0])
				cylinder(d=gear_hole_d, h=gear_h, $fn=60);
		}
	}
}

module AssemblySupport(
	arm_a = 36,
	arm_l = 16.5,
	arm_w = 4,
	bearing_d = 9.1,
	bearing_h = 7,
	bearing_margin = 2,
	motor_d = 18,
	motor_hole_d = 8,
	motor_margin = 3,
	motor_screw_d = 3.5,
	motor_screw_dist = 8.5,
	motor_th = 2.5,
	motor_y = 14.75,
	rod_d = 9,
	support_margin = 3,
	support_screw_d = 3,
	support_screw_dist = 8,
	support_screw_free = 1.5,
	support_th = 2.5,
	total_h = 25,
) {
	arm_x = cos(arm_a) * arm_l;
	arm_y = sin(arm_a) * arm_l;
	
	// Support
	difference() {
		union() {
			hull() {
				translate([support_screw_dist, 0, 0])
					cylinder(d=support_screw_d+2*support_margin, h=support_th, $fn=100);
				translate([-support_screw_dist, 0, 0])
					cylinder(d=support_screw_d+2*support_margin, h=support_th, $fn=100);
				cylinder(d=bearing_d+2*support_margin, h=support_th, $fn=100);
			}
			cylinder(d=bearing_d+2*bearing_margin, h=bearing_h, $fn=100);
			hull() {
				translate([arm_x, arm_y, 0])
					cylinder(d=arm_w, h=support_th, $fn=60);
				cylinder(d=arm_w, h=support_th, $fn=60);
			}
			hull() {
				translate([-arm_x, arm_y, 0])
					cylinder(d=arm_w, h=support_th, $fn=60);
				cylinder(d=arm_w, h=support_th, $fn=60);
			}
			hull() {
				translate([0, -arm_l, 0])
					cylinder(d=arm_w, h=support_th, $fn=60);
				cylinder(d=arm_w, h=support_th, $fn=60);
			}
		}
		
		translate([support_screw_dist, 0, 0])
			cylinder(d=support_screw_d, h=support_th, $fn=60);
		translate([-support_screw_dist, 0, 0])
			cylinder(d=support_screw_d, h=support_th, $fn=60);
		
		translate([support_screw_dist, 0, support_th])
			cylinder(d=support_screw_d+2*support_screw_free, h=bearing_h, $fn=60);
		translate([-support_screw_dist, 0, support_th])
			cylinder(d=support_screw_d+2*support_screw_free, h=bearing_h, $fn=60);
		
		cylinder(d=bearing_d, h=bearing_h, $fn=100);
	}
	
	// Motor
	translate([0, motor_y, total_h-motor_th]) difference() {
		union() {
			hull() {
				translate([motor_screw_dist, 0, 0])
					cylinder(d=motor_screw_d+2*motor_margin, h=motor_th, $fn=100);
				translate([-motor_screw_dist, 0, 0])
					cylinder(d=motor_screw_d+2*motor_margin, h=motor_th, $fn=100);
				cylinder(d=motor_hole_d+2*motor_margin, h=motor_th, $fn=100);
			}
			cylinder(d=motor_d, h=motor_th, $fn=100);
			hull() {
				translate([arm_x, arm_y-motor_y, 0])
					cylinder(d=arm_w, h=motor_th, $fn=60);
				cylinder(d=arm_w, h=motor_th, $fn=60);
			}
			hull() {
				translate([-arm_x, arm_y-motor_y, 0])
					cylinder(d=arm_w, h=motor_th, $fn=60);
				cylinder(d=arm_w, h=motor_th, $fn=60);
			}
			hull() {
				translate([0, -arm_l-motor_y, 0])
					cylinder(d=arm_w, h=motor_th, $fn=60);
				cylinder(d=arm_w, h=motor_th, $fn=60);
			}
			translate([0, -motor_y, 0])
				cylinder(d=rod_d+arm_w, h=motor_th, $fn=100);
		}
		
		translate([motor_screw_dist, 0, 0])
			cylinder(d=motor_screw_d, h=motor_th, $fn=60);
		translate([-motor_screw_dist, 0, 0])
			cylinder(d=motor_screw_d, h=motor_th, $fn=60);
		
		cylinder(d=motor_hole_d, h=motor_th, $fn=100);
		
		translate([0, -motor_y, 0])
			cylinder(d=rod_d, h=motor_th, $fn=60);
	}
	
	// Arms
	translate([arm_x, arm_y, 0])
		cylinder(d=arm_w, h=total_h, $fn=60);
	translate([-arm_x, arm_y, 0])
		cylinder(d=arm_w, h=total_h, $fn=60);
	translate([0, -arm_l, 0])
		cylinder(d=arm_w, h=total_h, $fn=60);
}

// Ancienne version utilisant le truc en métal du chariot d'origine pour maintenir l'axe
module CarriageUsingMetalThing(
	bar1_h = 6,
	bar1_screw_d = 2.4,
	bar1_screw_dist = 26,
	bar1_screw_z = 10.5,
	bar1_w = 6,
	bar1_y = 7.5,
	bar1_z = 12,
	belt_attach_base_h = 7,
	belt_attach_base_l = 21,
	belt_attach_base_w = 3.75,
	belt_attach_base_z = 4,
	belt_w = 3.5,
	belt_ystop_w = 1.5,
	encoder_col_bottom_th = 3,
	encoder_col_l = 9,
	encoder_col_w = 4,
	encoder_col_y = 5,
	encoder_slider_w = 12,
	encoder_slider_hole_d = 3.5,
	encoder_slider_margin = 2,
	encoder_slider_th = 2.5,
	encoder_slider_z = 33.5,
	endstop_screw_d = 3.5,
	endstop_screw_margin = 3,
	endstop_screw_y = 14,
	endstop_screw_z = 42,
	h = 49,
	l = 70,
	laser_screw_d = 3.5,
	laser_screw_dist = 20,
	laser_screw_margin = 3,
	laser_screw_y = 16,
	laser_screw_z1 = 42,
	laser_screw_z2 = 22,
	ribbon_h = 8.5,
	ribbon_w = 3,
	ribbon_y = 6,
	ribbon_z = 20,
	rod_d = 8.5,
	slider_d = 12,
	slider_l = 3,
	top_h = 5,
	top_mid_l = 13,
	top_mid_w = 3,
	top_side_dist = 20,
	top_side_l = 18,
	top_side_w = 3,
	top_space_w = 1.3,
	top_th = 3,
) {
	difference() {
		union() {
			rotate([0, 90, 0]) cylinder(d=slider_d, h=slider_l, $fn=60);
			rotate([0, 90, 0]) translate([0, 0, l-slider_l]) cylinder(d=slider_d, h=slider_l, $fn=60);
			
			translate([0, -slider_d/2, 0]) cube([slider_l, slider_d, h]);
			translate([l-slider_l, -slider_d/2, 0]) cube([slider_l, slider_d, h]);
		}
		rotate([0, 90, 0]) cylinder(d=rod_d, h=l, $fn=60);
		translate([0, -ribbon_y, ribbon_z]) cube([slider_l, ribbon_w, ribbon_h]);
		translate([l-slider_l, -ribbon_y, ribbon_z]) cube([slider_l, ribbon_w, ribbon_h]);
	}
	translate([0, 0, h]) {
		translate([l/2-top_mid_l/2, -top_space_w/2-top_mid_w, 0]) cube([top_mid_l, top_mid_w, top_h]);
		translate([l/2+top_side_dist/2, top_space_w/2, 0])
			cube([top_side_l, top_side_w, top_h]);
		translate([l/2-top_side_l-top_side_dist/2, top_space_w/2, 0])
			cube([top_side_l, top_side_w, top_h]);
	}
	translate([0, -slider_d/2, h-top_th]) cube([l, slider_d, top_th]);
	
	translate([l-slider_l, endstop_screw_y, endstop_screw_z]) difference() {
		union() {
			rotate([0, 90, 0]) cylinder(d=endstop_screw_d+2*endstop_screw_margin, h=slider_l, $fn=40);
			translate([0, -endstop_screw_y, -endstop_screw_d/2-endstop_screw_margin])
				cube([slider_l, endstop_screw_y, endstop_screw_d+2*endstop_screw_margin]);
		}
		rotate([0, 90, 0]) cylinder(d=endstop_screw_d, h=slider_l, $fn=40);
	}
	
	translate([0, laser_screw_y, laser_screw_z1]) difference() {
		union() {
			rotate([0, 90, 0]) cylinder(d=laser_screw_d+2*laser_screw_margin, h=slider_l, $fn=40);
			translate([0, -laser_screw_y, -laser_screw_d/2-laser_screw_margin])
				cube([slider_l, laser_screw_y, laser_screw_d+2*laser_screw_margin]);
		}
		rotate([0, 90, 0]) cylinder(d=laser_screw_d, h=slider_l, $fn=40);
	}
	translate([0, laser_screw_y, laser_screw_z2]) difference() {
		union() {
			rotate([0, 90, 0]) cylinder(d=laser_screw_d+2*laser_screw_margin, h=slider_l, $fn=40);
			translate([0, -laser_screw_y, -laser_screw_d/2-laser_screw_margin])
				cube([slider_l, laser_screw_y, laser_screw_d+2*laser_screw_margin]);
		}
		rotate([0, 90, 0]) cylinder(d=laser_screw_d, h=slider_l, $fn=40);
	}
	
	difference() {
		translate([0, -bar1_y, bar1_z-bar1_h])
			cube([l, bar1_w, bar1_h]);
		
		translate([l/2-bar1_screw_dist/2, -bar1_y, bar1_screw_z])
			rotate([-90, 0, 0]) cylinder(d=bar1_screw_d, h=bar1_w, $fn=40);
		translate([l/2+bar1_screw_dist/2, -bar1_y, bar1_screw_z])
			rotate([-90, 0, 0]) cylinder(d=bar1_screw_d, h=bar1_w, $fn=40);
	}
	translate([l/2-belt_attach_base_l/2, -bar1_y-belt_attach_base_w, belt_attach_base_z])
		cube([belt_attach_base_l, belt_attach_base_w, belt_attach_base_h]);
	
	translate([l/2-encoder_col_l/2, encoder_col_y, bar1_z])
		cube([encoder_col_l, encoder_col_w, h-bar1_z]);
	translate([l/2-encoder_col_l/2, bar1_w-bar1_y, bar1_z-encoder_col_bottom_th])
		cube([encoder_col_l, bar1_y-bar1_w+encoder_col_y+encoder_col_w, encoder_col_bottom_th]);
	translate([l/2, encoder_col_y, encoder_slider_z]) difference() {
		hull() {
			translate([-encoder_slider_hole_d/2-encoder_slider_margin, -encoder_slider_hole_d/2, 0]) cube([encoder_slider_hole_d+2*encoder_slider_margin, encoder_slider_hole_d/2, encoder_slider_th]);
			translate([0, -encoder_slider_w, 0]) cylinder(d=encoder_slider_hole_d+2*encoder_slider_margin, h=encoder_slider_th, $fn=40);
		}
		hull() {
			translate([0, -encoder_slider_hole_d/2, 0]) cylinder(d=encoder_slider_hole_d, h=encoder_slider_th, $fn=40);
			translate([0, -encoder_slider_w, 0]) cylinder(d=encoder_slider_hole_d, h=encoder_slider_th, $fn=40);
		}
	}
}

module Carriage(
	bar1_h = 6,
	bar1_w = 6,
	bar1_y = 7.5,
	bar1_z = 12,
	belt_chuck_static_th = 2,
	belt_chuck_static_l = 40,
	belt_chuck_static_w = 8,
	belt_chuck_static_y = 9.5,
	belt_chuck_static_z = 10, // at belt's flat side top
	encoder_col_bottom_th = 3,
	encoder_col_l = 9,
	encoder_col_w = 4,
	encoder_col_y = 5,
	encoder_slider_w = 12,
	encoder_slider_hole_d = 3.5,
	encoder_slider_margin = 2,
	encoder_slider_th = 2.5,
	encoder_slider_z = 33.5,
	endstop_screw_d = 3.5,
	endstop_screw_margin = 3,
	endstop_screw_y = 14,
	endstop_screw_z = 42,
	h = 49,
	l = 70,
	laser_screw_d = 3.5,
	laser_screw_dist = 20,
	laser_screw_margin = 3,
	laser_screw_y = 16,
	laser_screw_z1 = 42,
	laser_screw_z2 = 22,
	ribbon_h = 8.5,
	ribbon_w = 3,
	ribbon_y = 6,
	ribbon_z = 20,
	rod_d = 8.5,
	slider_d = 12,
	slider_l = 3,
	top_h = 5,
	top_mid_l = 13,
	top_mid_w = 3,
	top_side_dist = 20,
	top_side_l = 18,
	top_side_w = 3,
	top_space_w = 1.3,
	top_th = 3,
) {
	difference() {
		union() {
			rotate([0, 90, 0]) cylinder(d=slider_d, h=slider_l, $fn=60);
			rotate([0, 90, 0]) translate([0, 0, l-slider_l]) cylinder(d=slider_d, h=slider_l, $fn=60);
			
			translate([0, -slider_d/2, 0]) cube([slider_l, slider_d, h]);
			translate([l-slider_l, -slider_d/2, 0]) cube([slider_l, slider_d, h]);
		}
		rotate([0, 90, 0]) cylinder(d=rod_d, h=l, $fn=60);
		translate([0, -ribbon_y, ribbon_z]) cube([slider_l, ribbon_w, ribbon_h]);
		translate([l-slider_l, -ribbon_y, ribbon_z]) cube([slider_l, ribbon_w, ribbon_h]);
	}
	translate([0, 0, h]) {
		translate([l/2-top_mid_l/2, -top_space_w/2-top_mid_w, 0]) cube([top_mid_l, top_mid_w, top_h]);
		translate([l/2+top_side_dist/2, top_space_w/2, 0])
			cube([top_side_l, top_side_w, top_h]);
		translate([l/2-top_side_l-top_side_dist/2, top_space_w/2, 0])
			cube([top_side_l, top_side_w, top_h]);
	}
	translate([0, -slider_d/2, h-top_th]) cube([l, slider_d, top_th]);
	
	translate([l-slider_l, endstop_screw_y, endstop_screw_z]) difference() {
		union() {
			rotate([0, 90, 0]) cylinder(d=endstop_screw_d+2*endstop_screw_margin, h=slider_l, $fn=40);
			translate([0, -endstop_screw_y, -endstop_screw_d/2-endstop_screw_margin])
				cube([slider_l, endstop_screw_y, endstop_screw_d+2*endstop_screw_margin]);
		}
		rotate([0, 90, 0]) cylinder(d=endstop_screw_d, h=slider_l, $fn=40);
	}
	
	translate([0, laser_screw_y, laser_screw_z1]) difference() {
		union() {
			rotate([0, 90, 0]) cylinder(d=laser_screw_d+2*laser_screw_margin, h=slider_l, $fn=40);
			translate([0, -laser_screw_y, -laser_screw_d/2-laser_screw_margin])
				cube([slider_l, laser_screw_y, laser_screw_d+2*laser_screw_margin]);
		}
		rotate([0, 90, 0]) cylinder(d=laser_screw_d, h=slider_l, $fn=40);
	}
	translate([0, laser_screw_y, laser_screw_z2]) difference() {
		union() {
			rotate([0, 90, 0]) cylinder(d=laser_screw_d+2*laser_screw_margin, h=slider_l, $fn=40);
			translate([0, -laser_screw_y, -laser_screw_d/2-laser_screw_margin])
				cube([slider_l, laser_screw_y, laser_screw_d+2*laser_screw_margin]);
		}
		rotate([0, 90, 0]) cylinder(d=laser_screw_d, h=slider_l, $fn=40);
	}
	
	difference() {
		translate([0, -bar1_y, bar1_z-bar1_h])
			cube([l, bar1_w, bar1_h]);
	}
	
	translate([l/2-encoder_col_l/2, encoder_col_y, bar1_z])
		cube([encoder_col_l, encoder_col_w, h-bar1_z]);
	translate([l/2-encoder_col_l/2, bar1_w-bar1_y, bar1_z-encoder_col_bottom_th])
		cube([encoder_col_l, bar1_y-bar1_w+encoder_col_y+encoder_col_w, encoder_col_bottom_th]);
	translate([l/2, encoder_col_y, encoder_slider_z]) difference() {
		hull() {
			translate([-encoder_slider_hole_d/2-encoder_slider_margin, -encoder_slider_hole_d/2, 0]) cube([encoder_slider_hole_d+2*encoder_slider_margin, encoder_slider_hole_d/2, encoder_slider_th]);
			translate([0, -encoder_slider_w, 0]) cylinder(d=encoder_slider_hole_d+2*encoder_slider_margin, h=encoder_slider_th, $fn=40);
		}
		hull() {
			translate([0, -encoder_slider_hole_d/2, 0]) cylinder(d=encoder_slider_hole_d, h=encoder_slider_th, $fn=40);
			translate([0, -encoder_slider_w, 0]) cylinder(d=encoder_slider_hole_d, h=encoder_slider_th, $fn=40);
		}
	}
	
	translate([l/2-belt_chuck_static_l/2, -belt_chuck_static_y-belt_chuck_static_w, belt_chuck_static_z-belt_chuck_static_th])
		cube([belt_chuck_static_l, belt_chuck_static_w, belt_chuck_static_th]);
}

//AssemblySupport();
Carriage();
