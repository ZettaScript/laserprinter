use <pitch.scad>;

module Pulley(
	pulley_d1 = 48,
	pulley_d2 = 52,
	pulley_th1 = 0.5,
	pulley_th2 = 2,
	pulley_th3 = 1,
	rod_d = 4.8,
) {
	difference() {
		union() {
			translate([0, 0, -pulley_th1/2]) cylinder(d=pulley_d1, h=pulley_th1, $fn=100);
			hull() {
				translate([0, 0, -pulley_th1/2-pulley_th2])
					cylinder(d=pulley_d1, h=pulley_th2, $fn=100);
				translate([0, 0, -pulley_th1/2-pulley_th2-pulley_th3])
					cylinder(d=pulley_d2, h=pulley_th3, $fn=100);
			}
			mirror([0, 0, 1]) hull() {
				translate([0, 0, -pulley_th1/2-pulley_th2])
					cylinder(d=pulley_d1, h=pulley_th2, $fn=100);
				translate([0, 0, -pulley_th1/2-pulley_th2-pulley_th3])
					cylinder(d=pulley_d2, h=pulley_th3, $fn=100);
			}
		}
		translate([0, 0, -pulley_th1/2-pulley_th2-pulley_th3-1])
			cylinder(d=rod_d, h=pulley_th1+2*pulley_th2+2*pulley_th3+2, $fn=60);
	}
	translate([0, 0, pulley_th1/2+pulley_th2+pulley_th3+4.375])
		rod_pitch_m3(rod_d/2);
}

Pulley();
