use <foot.scad>;
use <laser.scad>;
use <mirror.scad>;
use <motor_holder.scad>;
use <pulley.scad>;
use <rolls.scad>;

module Motor() {
	color(c=[0.5, 0.5, 0.5, 0.5])
		cylinder(d=25.5, h=31, $fn=40);
	color(c=[0.2, 0.2, 0.2, 0.5])
		translate([0, 0, 31])
		cylinder(d=8, h=2, $fn=40);
	color(c=[0.5, 0.5, 0.5, 0.5])
		translate([0, 0, 31])
		cylinder(d=2, h=11, $fn=10);
	color(c=[1, 1, 1, 0.5])
		translate([0, 0, 37.5])
		cylinder(d=10, h=4, $fn=20);
}

translate([0, -80, -100]) rotate([0, 0, 90]) RodFoot();
translate([0, 160, -100]) rotate([0, 0, -90]) RodFoot();
translate([0, 0, 0]) rotate([90, 180, 0]) DemoRolls();
translate([0, 40, 0]) rotate([90, 180, 0]) DemoRolls();
translate([0, 60, 0]) rotate([90, 0, 0]) MirrorHolder(debug=true);
translate([0, 90, 0]) rotate([90, 0, 0]) Pulley();
translate([0, -40, -55]) rotate([0, 180, 90]) LaserRodHolder(debug=true);
translate([0, 95, -70]) rotate([0, 0 ,90]) MotorHolder();

translate([-5, -100, -67.25]) color(c=[0.5, 0.5, 0.5, 0.5]) cube([10, 300, 10]);
translate([0, 30, 0])
	rotate([-90, 0, 0])
	color(c=[0.5, 0.5, 0.5, 0.5])
	cylinder(d=4.5, h=100, $fn=20);
translate([0, 129.5, -38])
	rotate([90, 0, 0])
	Motor();
