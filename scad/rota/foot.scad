use <pitch.scad>;
use <rod.scad>;

module RodFoot(
	bolt_d = 3.2,
	bolt_h = 5.4,
	bolt_total_d = 8,
	bolt_y = 50,
	feet_l1 = 10,
	feet_l2 = 5,
	feet_h1 = 10,
	feet_h2 = 1.2,
	feet_w2 = 5,
	feet_y = 40,
	feet_z = 30,
	rod_holder_l = 20,
	rod_holder_th = 2,
	rod_w = 11,
) {
	translate([rod_holder_l/2, 0, feet_z])
		RodHolder(rod_holder_l=rod_holder_l, rod_holder_th=rod_holder_th, rod_w=rod_w, pitch_rot=-90);
	difference() {
		union() {
			translate([0, rod_w/2, 0]) hull() {
				translate([0, 0, feet_z]) cube([feet_l1, rod_holder_th, feet_h1]);
				translate([0, feet_y, 0]) cube([feet_l2, feet_w2, feet_h2]);
			}
			translate([bolt_total_d/2, bolt_y, 0]) cylinder(d=bolt_total_d, h=bolt_h, $fn=40);
		}
		translate([bolt_total_d/2, bolt_y, -1]) cylinder(d=bolt_d, h=bolt_h+2, $fn=20);
	}
	mirror([0, 1, 0]) difference() {
		union() {
			translate([0, rod_w/2, 0]) hull() {
				translate([0, 0, feet_z]) cube([feet_l1, rod_holder_th, feet_h1]);
				translate([0, feet_y, 0]) cube([feet_l2, feet_w2, feet_h2]);
			}
			translate([bolt_total_d/2, bolt_y, 0]) cylinder(d=bolt_total_d, h=bolt_h, $fn=40);
		}
		translate([bolt_total_d/2, bolt_y, -1]) cylinder(d=bolt_d, h=bolt_h+2, $fn=20);
	}
}

RodFoot();
