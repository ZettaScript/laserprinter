use <pitch.scad>;
use <rod.scad>;

module LaserRodHolder(
	arm_l = 30,
	arm_w = 10,
	ball_arm_d = 5,
	ball_d = 16,
	ball_z = 4,
	rod_holder_l = 20,
	rod_holder_th = 2,
	rod_w = 11,
	debug = false,
) {
	translate([0, rod_holder_th+rod_w/2, rod_holder_th+rod_w/2])
		rotate([90, 0, 0])
		RodHolder(rod_holder_l=rod_holder_l, rod_holder_th=rod_holder_th, rod_w=rod_w);
	translate([0, 0, -ball_z-ball_d/2])
		cylinder(d=ball_arm_d, h=ball_d/2+ball_z, $fn=60);
	translate([0, 0, -ball_z-ball_d/2])
		sphere(d=ball_d, $fn=100);
	if(debug) {
		translate([0, 0, -ball_z-ball_d/2+1])
			color("orange") LaserBallBlocker();
		/*translate([0, 0, -ball_z-ball_d/2-1])
			color("orange") LaserBallCup();
		translate([0, 0, -ball_d-ball_d/2-1-15])
			LaserHolder();*/
		translate([0, 0, -ball_z-ball_d/2-1])
			color("orange") LaserBallCup2();
	}
}

module LaserBallBlocker(
	aperture_w = 6.5,
	arm_bolt_dist = 32,
	arm_rot = 28,
	arm_th = 3,
	arm_w = 9.5,
	free_d = 10,
	inner_d = 16.4,
	outer_d = 19.6,
	th = 2,
) {
	difference() {
		union() {
			sphere(d=outer_d, $fn=100);
			rotate([0, 0, arm_rot]) hull() {
				translate([0, arm_bolt_dist/2, 0]) cylinder(d=arm_w, h=arm_th, $fn=40);
				translate([0, -arm_bolt_dist/2, 0]) cylinder(d=arm_w, h=arm_th, $fn=40);
			}
		}
		sphere(d=inner_d, $fn=100);
		translate([-outer_d/2, -outer_d/2, -outer_d]) cube([outer_d, outer_d, outer_d]);
		cylinder(d=free_d, h=outer_d, $fn=60);
		translate([0, 0, -1]) hull() {
			cylinder(d=aperture_w, h=outer_d, $fn=60);
			translate([outer_d, 0, 0])
				cylinder(d=aperture_w, h=outer_d, $fn=60);
		}
		rotate([0, 0, arm_rot]) translate([0, arm_bolt_dist/2, -1]) cylinder(d=3.2, h=arm_th+2, $fn=40);
		rotate([0, 0, arm_rot]) translate([0, -arm_bolt_dist/2, -1]) cylinder(d=3.2, h=arm_th+2, $fn=40);
	}
}

module LaserBallCup(
	arm_bolt_dist = 32,
	arm_rot = 28,
	arm_th = 3,
	arm_w = 9.5,
	inner_d = 16.4,
	outer_d = 19.6,
	slider_inner_d = 3.2,
	slider_l = 8,
	slider_outer_d = 8,
	slider_th = 2,
) {
	difference() {
		union() {
			sphere(d=outer_d, $fn=100);
			rotate([0, 0, arm_rot]) hull() {
				translate([0, arm_bolt_dist/2, -arm_th]) cylinder(d=arm_w, h=arm_th, $fn=40);
				translate([0, -arm_bolt_dist/2, -arm_th]) cylinder(d=arm_w, h=arm_th, $fn=40);
			}
			hull() {
				rotate([0, 90, 0])
					translate([0, 0, -slider_th/2])
					cylinder(d=slider_outer_d, h=slider_th, $fn=60);
				translate([0, 0, -slider_l-outer_d/2])
					rotate([0, 90, 0])
					translate([0, 0, -slider_th/2])
					cylinder(d=slider_outer_d, h=slider_th, $fn=60);
			}
		}
		sphere(d=inner_d, $fn=100);
		translate([-outer_d/2, -outer_d/2, 0]) cube([outer_d, outer_d, outer_d]);
		rotate([0, 0, arm_rot]) translate([0, arm_bolt_dist/2, -arm_th-1]) cylinder(d=3.2, h=arm_th+2, $fn=40);
		rotate([0, 0, arm_rot]) translate([0, -arm_bolt_dist/2, -arm_th-1]) cylinder(d=3.2, h=arm_th+2, $fn=40);
		hull() {
			translate([0, 0, -outer_d/2-slider_inner_d/2])
				rotate([0, 90, 0])
				translate([0, 0, -slider_th/2-1])
				cylinder(d=slider_inner_d, h=slider_th+1, $fn=60);
			translate([0, 0, -slider_l-outer_d/2])
				rotate([0, 90, 0])
				translate([0, 0, -slider_th/2-1])
				cylinder(d=slider_inner_d, h=slider_th+2, $fn=60);
		}
	}
	rotate([0, 0, arm_rot]) translate([0, arm_bolt_dist/2, -arm_th]) rotate([0, 180, 90]) simple_pitch_m3(h1=1, h2=0);
	rotate([0, 0, arm_rot]) translate([0, -arm_bolt_dist/2, -arm_th]) rotate([0, 180, 90]) simple_pitch_m3(h1=1, h2=0);
}

module LaserHolder(
	l = 46,
	laser_bolt_dx = 20,
	laser_bolt_dy = 40,
	arm_dist = 2.4,
	arm_h = 9,
	arm_th = 2,
	arm_w = 8,
	th = 2,
	w = 26,
) {
	rotate([0, 0, 90]) difference() {
		translate([-w/2, -l/2, 0]) cube([w, l, th]);
		translate([laser_bolt_dx/2, laser_bolt_dy/2, -1]) cylinder(d=3.2, h=th+2, $fn=40);
		translate([laser_bolt_dx/2, -laser_bolt_dy/2, -1]) cylinder(d=3.2, h=th+2, $fn=40);
		translate([-laser_bolt_dx/2, laser_bolt_dy/2, -1]) cylinder(d=3.2, h=th+2, $fn=40);
		translate([-laser_bolt_dx/2, -laser_bolt_dy/2, -1]) cylinder(d=3.2, h=th+2, $fn=40);
	}
	difference() {
		hull() {
			translate([-arm_th-arm_dist/2, -arm_w/2, 0]) cube([arm_th*2+arm_dist, arm_w, th]);
			translate([0, 0, th+arm_h])
				rotate([0, 90, 0])
				translate([0, 0, -arm_th-arm_dist/2])
				cylinder(d=arm_w, h=arm_th*2+arm_dist, $fn=60);
		}
		translate([0, 0, th+arm_h])
			rotate([0, 90, 0])
			translate([0, 0, -arm_th-arm_dist/2-1])
			cylinder(d=3.2, h=arm_th*2+arm_dist+2, $fn=40);
		translate([-arm_dist/2, -arm_w/2-1, th]) cube([arm_dist, arm_w+2, arm_h+arm_w]);
	}
	translate([arm_th+arm_dist/2, 0, th+arm_h])
		rotate([0, 90, 0])
		simple_pitch_m3();
}

module LaserBallCup2(
	arm_bolt_dist = 32,
	arm_rot = 28,
	arm_th = 3,
	arm_w = 9.5,
	foot_l1 = 10,
	foot_l2 = 32,
	foot_th = 2,
	foot_w1 = 10,
	foot_w2 = 23,
	inner_d = 16.4,
	laser_bolt_dx = 16,
	laser_bolt_dy = 40,
	laser_plate_l = 46,
	laser_plate_th = 2,
	laser_plate_w = 23,
	laser_plate_z = 10,
	outer_d = 19.6,
) {
	difference() {
		union() {
			sphere(d=outer_d, $fn=100);
			rotate([0, 0, arm_rot]) hull() {
				translate([0, arm_bolt_dist/2, -arm_th]) cylinder(d=arm_w, h=arm_th, $fn=40);
				translate([0, -arm_bolt_dist/2, -arm_th]) cylinder(d=arm_w, h=arm_th, $fn=40);
			}
			hull() {
				translate([-foot_l1/2, -foot_th/2, -foot_th])
					cube([foot_l1, foot_th, foot_th]);
				translate([-foot_l2/2, -foot_th/2, -laser_plate_th-laser_plate_z])
					cube([foot_l2, foot_th, laser_plate_th]);
			}
			hull() {
				translate([-foot_th/2, -foot_w1/2, -foot_th])
					cube([foot_th, foot_w1, foot_th]);
				translate([-foot_th/2, -foot_w2/2, -laser_plate_th-laser_plate_z])
					cube([foot_th, foot_w2, laser_plate_th]);
			}
		}
		sphere(d=inner_d, $fn=100);
		translate([-outer_d/2, -outer_d/2, 0]) cube([outer_d, outer_d, outer_d]);
		rotate([0, 0, arm_rot]) translate([0, arm_bolt_dist/2, -arm_th-1]) cylinder(d=3.2, h=arm_th+2, $fn=40);
		rotate([0, 0, arm_rot]) translate([0, -arm_bolt_dist/2, -arm_th-1]) cylinder(d=3.2, h=arm_th+2, $fn=40);
	}
	rotate([0, 0, arm_rot]) translate([0, arm_bolt_dist/2, -arm_th]) rotate([0, 180, -90]) simple_pitch_m3(h1=1, h2=0);
	rotate([0, 0, arm_rot]) translate([0, -arm_bolt_dist/2, -arm_th]) rotate([0, 180, 90]) simple_pitch_m3(h1=1, h2=0);
	translate([0, 0, -laser_plate_z-laser_plate_th]) rotate([0, 0, 90]) difference() {
		translate([-laser_plate_w/2, -laser_plate_l/2, 0])
			cube([laser_plate_w, laser_plate_l, laser_plate_th]);
		translate([laser_bolt_dx/2, laser_bolt_dy/2, -1]) cylinder(d=3.2, h=laser_plate_th+2, $fn=40);
		translate([laser_bolt_dx/2, -laser_bolt_dy/2, -1]) cylinder(d=3.2, h=laser_plate_th+2, $fn=40);
		translate([-laser_bolt_dx/2, laser_bolt_dy/2, -1]) cylinder(d=3.2, h=laser_plate_th+2, $fn=40);
		translate([-laser_bolt_dx/2, -laser_bolt_dy/2, -1]) cylinder(d=3.2, h=laser_plate_th+2, $fn=40);
		translate([0, laser_bolt_dy/2, -1]) cylinder(d=3.2, h=laser_plate_th+2, $fn=40);
		translate([0, -laser_bolt_dy/2, -1]) cylinder(d=3.2, h=laser_plate_th+2, $fn=40);
	}
}

LaserRodHolder(debug=true);
