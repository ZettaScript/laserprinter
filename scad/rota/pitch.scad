/*
 * CopyLeft 2017-2022 Pascal Engélibert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

// Ideal values but may increase depending on printer/slicer settings
// Please print tests to fine tune this parameter
M3_RAY = 1.725; // M3 bolt ray (diameter=3mm) Ideal=1.5


// Functions are accessible outside scope, not variables
function M3_RAY() = M3_RAY;

module hexagone(l,h) {
	a = l/tan(60);
	union() {
		rotate([0,0,0]) cube([l,a,h],center=true);
		rotate([0,0,60]) cube([l,a,h],center=true);
		rotate([0,0,120]) cube([l,a,h],center=true);
	}
}

// added h:0.6 w:0.25
// b:add bottom nut support
// s:bottom support height
module pitch_m2_5(b=0,s=0) {
	translate([0,0,(b+s)/2]) difference() {
		hexagone(7.25,b+s);
		cylinder(h=1+b+s,r=1.375,center=true,$fn=20);
		if(s>0) {
			translate([0,0,-b]) hexagone(5.25,b+s);
			translate([-2.625,0,-(b+s)/2-b]) cube([5.25,5.25,b+s]);
		}
	}
	translate([0,0,1.8+b+s]) difference() {
		hexagone(7.25,3.6);
		cylinder(h=4.6,r=1.375,center=true,$fn=20);
		translate([0,0,-1]) hexagone(5.25,3.6);
		translate([-2.625,0,-2.8]) cube([5.25,5.25,3.6]);
	}
}

// b:add bottom nut support
// s:bottom support height
// h:head support additionnal thickness
// w:side additionnal thickness
module pitch_m3(b=0,s=0,h=0,w=0) {
	translate([0,0,(b+s)/2]) difference() {
		hexagone(7.75+w,b+s);
		cylinder(h=1+b+s,r=M3_RAY,center=true,$fn=20);
		if(s>0) {
			translate([0,0,-b]) hexagone(5.75,b+s);
			translate([-2.875,0,-(b+s)/2-b]) cube([5.75,5.75,b+s]);
		}
	}
	translate([0,0,2+b+s+h/2]) difference() {
		hexagone(7.75+w,4+h);
		cylinder(h=5+h,r=M3_RAY,center=true,$fn=20);
		translate([0,0,-1]) hexagone(5.75,4);
		translate([-2.875,0,-3]) cube([5.75,5.75,4]);
	}
}

module pitch_inv_m3(y=0, z1=0, z2=0, hp=0) {
	translate([0,0,2-hp/2]) hexagone(5.75, 4+hp);
	translate([-3,0,-hp]) cube([6, y, 4+hp]);
	translate([0,0,-z2-hp]) cylinder(r=M3_RAY, h=z1+4+z2+hp, $fn=20);
}

module simple_pitch_m3(th=1, h1=1, h2=1, w) {
	difference() {
		translate([0, 0, 2+h1/2-h2/2]) hexagone(7+th, 4+h1+h2);
		pitch_inv_m3(6+th, h1+1, h2+1);
	}
}

// a:rod radius
// t:thickness
module rod_pitch_m3(a, t=2, center=true) {
	translate([0,0,center?0:4.375]) difference() {
		union() {
			cylinder(h=8.75, r=a+t, center=true, $fn=60);
			rotate([0,90,0]) pitch_m3(a+t,0,1,1);
		}
		cylinder(h=9, r=a, center=true, $fn=60);
		rotate([0,90,0]) cylinder(h=a+t+1, r=M3_RAY, $fn=20);
	}
}

module side_pitch_m3(th=1.2, h1=1, h2=2) {
	difference() {
		translate([0,0,2+h1/2-h2/2]) hexagone(7+th, 4+h1+h2);
		pitch_inv_m3(6+th, h1+1, h2+1, h1);
	}
}

//rod_pitch_m3(3);
side_pitch_m3();
