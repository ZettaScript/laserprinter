use <pitch.scad>;

module RodHolder(
	rod_holder_l = 20,
	rod_holder_th = 2,
	rod_w = 11,
	wedge_margin = 2,
	pitch_rot = 0,
) {
	difference() {
		translate([-rod_holder_l/2, -rod_holder_th-rod_w/2, 0])
			cube([rod_holder_l, rod_w+2*rod_holder_th, rod_w+2*rod_holder_th+wedge_margin]);
		translate([-rod_holder_l/2-1, -rod_w/2, rod_holder_th])
			cube([rod_holder_l+2, rod_w, rod_w+wedge_margin]);
		translate([0, 0, rod_holder_th+rod_w+wedge_margin-1])
			cylinder(d=3.2, h=rod_holder_th+2, $fn=20);
	}
	translate([0, 0, 2*rod_holder_th+rod_w+wedge_margin])
		rotate([0, 0, pitch_rot])
		simple_pitch_m3(th=1.4);
}

module Wedge(
	hole_d = 4,
	hole_th = 0.5,
	l = 9,
	th = 1.5,
	w = 20,
) {
	difference() {
		translate([-l/2, -w/2, 0])
			cube([l, w, th]);
		translate([0, 0, th-hole_th])
			cylinder(d=hole_d, h=hole_th+1, $fn=20);
	}
}

Wedge();
