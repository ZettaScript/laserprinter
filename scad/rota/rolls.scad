use <pitch.scad>;
use <rod.scad>;

M3_FIXED = 3.0;
M3_FREE = 3.3;

INNER_RING_D = 58;
INNER_RING_TH = 4;
OUTER_RING_D = 108;
PAPER_WHEEL_HOLDER_ATTACH_ARM_L = 15;
PAPER_WHEEL_HOLDER_ATTACH_ARM_TH = 1.5;
PAPER_WHEEL_HOLDER_ATTACH_L = 3;
PAPER_WHEEL_HOLDER_MID_TH = 2.5;
PAPER_WHEEL_HOLDER_SPRING_D = 2;
PAPER_WHEEL_HOLDER_SPRING_H = 2;
PAPER_WHEEL_HOLDER_TH = 6.5;
PAPER_WHEEL_HOLDER_TOP_A = 23;
PAPER_WHEEL_HOLDER_WHEEL_ARM_L = 13.5;
PAPER_WHEEL_HOLDER_WHEEL_ARM_TH = 2;
PAPER_WHEEL_HOLDER_WHEEL_R_M = 9.5;
PAPER_WHEEL_HOLDER_WHEEL_TH_M = 5.5;
PAPER_WHEEL_NB = 10;
SHEET_L = 297;
SHEET_L_MARGIN = 2;

module OuterRing(
	ring_d = OUTER_RING_D, // inner diameter of outer ring
	ring_th = 4,
	ring_w = 6,
	rod_holder_l = 15,
	rod_holder_th = 2,
	rod_holder_z = 62.25,
	rod_w = 11,
	sheet_l = SHEET_L,
	sheet_l_blocker_h = 3,
	sheet_l_blocker_th = 2,
	sheet_l_margin = SHEET_L_MARGIN,
	sheet_w = 210,
	wedge_margin = 2,
) {
	top_p = PI * ring_d - sheet_l - sheet_l_margin;
	top_a = 360 * top_p / (top_p + sheet_l + sheet_l_margin);
	
	translate([0, 0, -ring_w/2]) difference() {
		cylinder(h=ring_w, d=ring_d + 2*ring_th, $fn=200);
		translate([0, 0, -1]) cylinder(h=ring_w+2, d=ring_d, $fn=200);
		//rotate([0, 0, 90-top_a/2]) linear_extrude(height=ring_th) polygon([[0, 0], [ring_d, 0], [ring_d*cos(top_a), ring_d*sin(top_a)]]);
		translate([-rod_w/2, -rod_w/2+rod_holder_z, -1])
		cube([rod_w+wedge_margin, rod_w, ring_w+2]);
	}
	
	rotate([0, 0, 90-top_a/2]) translate([ring_d/2-sheet_l_blocker_h, 0, -ring_w/2]) cube([sheet_l_blocker_h+ring_th, sheet_l_blocker_th, ring_w]);
	rotate([0, 0, 90+top_a/2]) translate([ring_d/2-sheet_l_blocker_h, -sheet_l_blocker_th, -ring_w/2]) cube([sheet_l_blocker_h+ring_th, sheet_l_blocker_th, ring_w]);
	
	translate([-rod_holder_th-rod_w/2, rod_holder_z, rod_holder_l/2-ring_w/2])
		rotate([0, 90, 0])
		RodHolder(rod_holder_l=rod_holder_l, rod_holder_th=rod_holder_th, rod_w=rod_w, pitch_rot=90);
}

module InnerRing(
	inner_ring_d = INNER_RING_D, // outer diameter of inner ring
	inner_ring_th = INNER_RING_TH,
	inner_ring_w = 12,
	ring_d = OUTER_RING_D,
	sheet_l = SHEET_L,
	sheet_l_margin = SHEET_L_MARGIN,
	paper_wheel_holder_hole_dist = 5,
	paper_wheel_holder_hole_h = 8,
	paper_wheel_holder_hole_w = 4.2,
	paper_wheel_holder_spring_d = PAPER_WHEEL_HOLDER_SPRING_D,
	paper_wheel_holder_spring_h = PAPER_WHEEL_HOLDER_SPRING_H,
	top_a = PAPER_WHEEL_HOLDER_TOP_A,
	wheel_nb = PAPER_WHEEL_NB,
) {
	outer_points = [
		for(i = [0 : wheel_nb]) [
			inner_ring_d/2*cos(top_a/2 + i*(360-top_a)/wheel_nb),
			inner_ring_d/2*sin(top_a/2 + i*(360-top_a)/wheel_nb)
		]
	];
	inner_points = [
		for(i = [0 : wheel_nb]) [
			(inner_ring_d/2-inner_ring_th)*cos(top_a/2 + i*(360-top_a)/wheel_nb),
			(inner_ring_d/2-inner_ring_th)*sin(top_a/2 + i*(360-top_a)/wheel_nb)
		]
	];
	
	difference() {
		translate([0, 0, -inner_ring_w/2]) rotate([0, 0, 90]) difference() {
			linear_extrude(inner_ring_w) polygon(outer_points);
			translate([0, 0, -1]) linear_extrude(inner_ring_w+2) polygon(inner_points);
		}
		
		for(i = [0 : wheel_nb-1]) {
			rotate([0, 0, top_a/2 + (i+0.5)*(360-top_a)/wheel_nb])
				translate([0, (inner_ring_d/2-inner_ring_th)*sqrt(0.5+cos((360-top_a)/wheel_nb)/2)-1, -paper_wheel_holder_hole_h/2]) {
					translate([paper_wheel_holder_hole_dist/2, 0, 0])
						cube([paper_wheel_holder_hole_w, inner_ring_th+2, paper_wheel_holder_hole_h]);
					translate([-paper_wheel_holder_hole_dist/2-paper_wheel_holder_hole_w, 0, 0])
						cube([paper_wheel_holder_hole_w, inner_ring_th+2, paper_wheel_holder_hole_h]);
				}
		}
	}
	
	for(i = [0 : wheel_nb-1]) {
		rotate([0, 0, top_a/2 + (i+0.5)*(360-top_a)/wheel_nb])
			translate([0, inner_ring_d*sin(top_a/2 + (360-top_a)/wheel_nb/2), 0])
			rotate([-90, 0, 0])
			cylinder(d=paper_wheel_holder_spring_d, h=paper_wheel_holder_spring_h, $fn=20);
	}
}

// TODO inner & outer wheels with rounded perimeter to fit big ring's curvature
// but maybe not, because it would not roll uniformly
module PaperWheel(,
	d = 15,
	hole_d = M3_FREE,
	th = 4,
) {
	translate([0, 0, -th/2]) difference() {
		cylinder(h=th, d=d, $fn=80);
		translate([0, 0, -1]) cylinder(h=th+2, d=hole_d, $fn=40);
	}
}

module PaperWheelHolder(
	attach_arm_l = PAPER_WHEEL_HOLDER_ATTACH_ARM_L,
	attach_arm_th = PAPER_WHEEL_HOLDER_ATTACH_ARM_TH,
	attach_h = 1.8,
	attach_l = PAPER_WHEEL_HOLDER_ATTACH_L,
	attach_th = 0.5,
	mid_th = PAPER_WHEEL_HOLDER_MID_TH,
	spring_d = PAPER_WHEEL_HOLDER_SPRING_D,
	spring_h = PAPER_WHEEL_HOLDER_SPRING_H,
	th = PAPER_WHEEL_HOLDER_TH,
	wheel_arm_l = PAPER_WHEEL_HOLDER_WHEEL_ARM_L,
	wheel_arm_th = PAPER_WHEEL_HOLDER_WHEEL_ARM_TH,
	wheel_r_m = PAPER_WHEEL_HOLDER_WHEEL_R_M,
	wheel_th_m = PAPER_WHEEL_HOLDER_WHEEL_TH_M,
) {
	max_arm_th = max(attach_arm_th, wheel_arm_th);
	translate([-wheel_th_m/2, 0, -th/2]) cube([wheel_th_m, mid_th, th]);
	translate([wheel_th_m/2+max_arm_th-wheel_arm_th, 0, -th/2]) difference() {
		cube([wheel_arm_th, wheel_arm_l+mid_th, th]);
		translate([-1, wheel_r_m+mid_th, th/2]) rotate([0, 90, 0]) cylinder(d=M3_FREE, h=wheel_arm_th+2, $fn=40);
	}
	translate([-max_arm_th-wheel_th_m/2, 0, -th/2]) difference() {
		cube([wheel_arm_th, wheel_arm_l+mid_th, th]);
		translate([-1, wheel_r_m+mid_th, th/2]) rotate([0, 90, 0]) cylinder(d=M3_FIXED, h=wheel_arm_th+2, $fn=40);
	}
	translate([wheel_th_m/2+max_arm_th-attach_arm_th, -attach_arm_l, -th/2]) union() {
		cube([attach_arm_th, attach_arm_l+mid_th, th]);
		hull() {
			translate([-attach_h, attach_l-attach_th, 0]) cube([attach_arm_th+attach_h, attach_th, th]);
			cube([attach_arm_th, attach_l, th]);
		}
	}
	translate([-max_arm_th-wheel_th_m/2, -attach_arm_l, -th/2]) union() {
		cube([attach_arm_th, attach_arm_l+mid_th, th]);
		hull() {
			translate([0, attach_l-attach_th, 0]) cube([attach_arm_th+attach_h, attach_th, th]);
			cube([attach_arm_th, attach_l, th]);
		}
	}
	rotate([90, 0, 0]) cylinder(d=spring_d, h=spring_h, $fn=40);
}

TOP_P = PI * OUTER_RING_D - SHEET_L - SHEET_L_MARGIN;
TOP_A = 360 * TOP_P / (TOP_P + SHEET_L + SHEET_L_MARGIN);
PAPER_WHEEL_MAX_ARM_TH = max(PAPER_WHEEL_HOLDER_WHEEL_ARM_TH, PAPER_WHEEL_HOLDER_ATTACH_ARM_TH);

module DemoRolls() {
	OuterRing();
	InnerRing();
	for(i = [0 : PAPER_WHEEL_NB-1]) {
		rotate([0, 0, PAPER_WHEEL_HOLDER_TOP_A/2+(i+0.5)*(360-PAPER_WHEEL_HOLDER_TOP_A)/PAPER_WHEEL_NB])
			translate([0, PAPER_WHEEL_HOLDER_ATTACH_ARM_L-PAPER_WHEEL_HOLDER_ATTACH_L+(INNER_RING_D/2-INNER_RING_TH)*cos((360-PAPER_WHEEL_HOLDER_TOP_A)/PAPER_WHEEL_NB/2)-2, 0]) {
				PaperWheelHolder();
				translate([0, PAPER_WHEEL_HOLDER_MID_TH+PAPER_WHEEL_HOLDER_WHEEL_R_M]) rotate([0, 90, 0]) PaperWheel();
			}
	}
}

DemoRolls();
