use <pitch.scad>;
use <rod.scad>;

module MotorHolder(
	arm_th = 2.4,
	arm_w = 15,
	bearing_arm_l = 8,
	bearing_arm_th = 2,
	bearing_d = 11.4,
	bearing_l = 8,
	bearing_offset = 0.4,
	bearing_th = 2.4,
	bearing_z = 70,
	feet_h = 5,
	feet_th = 2,
	motor_bolt_d = 3.5,
	motor_bolt_dist = 17,
	motor_bolt_slider_h = 10,
	motor_bolt_slider_margin = 4,
	motor_bolt_slider_z = 16,
	rod_holder_l = 20,
	rod_holder_th = 2,
	rod_w = 11,
	shaft_d = 9,
) {
	translate([rod_holder_l/2, rod_holder_th+rod_w/2, rod_holder_th+rod_w/2])
		rotate([90, 0, 0])
		RodHolder(rod_holder_l=rod_holder_l, rod_holder_th=rod_holder_th, rod_w=rod_w);
	difference() {
		union() {
			hull() {
				translate([0, -motor_bolt_dist/2, 2*rod_holder_th+rod_w+motor_bolt_slider_z])
					rotate([0, 90, 0])
					cylinder(d=motor_bolt_d+2*motor_bolt_slider_margin, h=arm_th, $fn=40);
				translate([0, -motor_bolt_dist/2, 2*rod_holder_th+rod_w+motor_bolt_slider_z+motor_bolt_slider_h])
					rotate([0, 90, 0])
					cylinder(d=motor_bolt_d+2*motor_bolt_slider_margin, h=arm_th, $fn=40);
				translate([0, motor_bolt_dist/2, 2*rod_holder_th+rod_w+motor_bolt_slider_z])
					rotate([0, 90, 0])
					cylinder(d=motor_bolt_d+2*motor_bolt_slider_margin, h=arm_th, $fn=40);
				translate([0, motor_bolt_dist/2, 2*rod_holder_th+rod_w+motor_bolt_slider_z+motor_bolt_slider_h])
					rotate([0, 90, 0])
					cylinder(d=motor_bolt_d+2*motor_bolt_slider_margin, h=arm_th, $fn=40);
			}
			translate([0, -arm_w/2, 2*rod_holder_th+rod_w])
				cube([arm_th, arm_w, bearing_z-2*rod_holder_th-rod_w]);
			translate([0, 0, bearing_z])
				rotate([0, 90, 0])
				cylinder(d=bearing_d+2*bearing_th, h=bearing_l, $fn=80);
			translate([0, -bearing_d/2-bearing_th-bearing_arm_l, bearing_z-bearing_offset-bearing_arm_th])
				cube([bearing_l, 2*bearing_arm_l+2*bearing_th+bearing_d, bearing_arm_th]);
		}
		hull() {
			translate([-1, 0, 2*rod_holder_th+rod_w+motor_bolt_slider_z])
				rotate([0, 90, 0])
				cylinder(d=shaft_d, h=arm_th+2, $fn=40);
			translate([-1, 0, 2*rod_holder_th+rod_w+motor_bolt_slider_z+motor_bolt_slider_h])
				rotate([0, 90, 0])
				cylinder(d=shaft_d, h=arm_th+2, $fn=40);
		}
		hull() {
			translate([-1, -motor_bolt_dist/2, 2*rod_holder_th+rod_w+motor_bolt_slider_z])
				rotate([0, 90, 0])
				cylinder(d=motor_bolt_d, h=arm_th+2, $fn=40);
			translate([-1, -motor_bolt_dist/2, 2*rod_holder_th+rod_w+motor_bolt_slider_z+motor_bolt_slider_h])
				rotate([0, 90, 0])
				cylinder(d=motor_bolt_d, h=arm_th+2, $fn=40);
		}
		hull() {
			translate([-1, motor_bolt_dist/2, 2*rod_holder_th+rod_w+motor_bolt_slider_z])
				rotate([0, 90, 0])
				cylinder(d=motor_bolt_d, h=arm_th+2, $fn=40);
			translate([-1, motor_bolt_dist/2, 2*rod_holder_th+rod_w+motor_bolt_slider_z+motor_bolt_slider_h])
				rotate([0, 90, 0])
				cylinder(d=motor_bolt_d, h=arm_th+2, $fn=40);
		}
		translate([-1, 0, bearing_z])
			rotate([0, 90, 0])
			cylinder(d=bearing_d, h=bearing_l+2, $fn=80);
		translate([-1, -bearing_d/2-bearing_th, bearing_z-bearing_offset])
			cube([bearing_l+2, bearing_d+2*bearing_th, bearing_d+bearing_th+bearing_offset]);
		translate([bearing_l/2, -bearing_d/2-bearing_th-bearing_arm_l/2, bearing_z-bearing_offset-bearing_arm_th-1])
			cylinder(d=3.2, h=bearing_arm_th+2, $fn=20);
		mirror([0, 1, 0])
			translate([bearing_l/2, -bearing_d/2-bearing_th-bearing_arm_l/2, bearing_z-bearing_offset-bearing_arm_th-1])
			cylinder(d=3.2, h=bearing_arm_th+2, $fn=20);
	}
	hull() {
		translate([0, -arm_w/2, rod_holder_th+rod_w])
			cube([arm_th, feet_th, rod_holder_th+feet_h]);
		translate([0, -arm_w/2, rod_holder_th+rod_w])
			cube([rod_holder_l, feet_th, rod_holder_th]);
	}
	mirror([0, 1, 0])hull() {
		translate([0, -arm_w/2, rod_holder_th+rod_w])
			cube([arm_th, feet_th, rod_holder_th+feet_h]);
		translate([0, -arm_w/2, rod_holder_th+rod_w])
			cube([rod_holder_l, feet_th, rod_holder_th]);
	}
}

MotorHolder();
