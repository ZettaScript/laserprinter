use <pitch.scad>;

module MirrorFrontFrame(
	h = 2,
	inner_w = 13,
	mirror_bolt_d = 3.2,
	mirror_bolt_dist = 21,
	outer_w = 29,
) {
	difference() {
		cube([outer_w, outer_w, h]);
		translate([(outer_w-inner_w)/2, (outer_w-inner_w)/2, -1])
			cube([inner_w, inner_w, h+2]);
		translate([outer_w/2+mirror_bolt_dist/2, outer_w/2+mirror_bolt_dist/2, -1])
			cylinder(d=mirror_bolt_d, h=h+2, $fn=20);
		translate([outer_w/2+mirror_bolt_dist/2, outer_w/2-mirror_bolt_dist/2, -1])
			cylinder(d=mirror_bolt_d, h=h+2, $fn=20);
		translate([outer_w/2-mirror_bolt_dist/2, outer_w/2, -1])
			cylinder(d=mirror_bolt_d, h=h+2, $fn=20);
	}
	translate([outer_w/2+mirror_bolt_dist/2, outer_w/2+mirror_bolt_dist/2, h])
		rotate([0, 0, -90])
		simple_pitch_m3(h1=1, h2=0);
	translate([outer_w/2+mirror_bolt_dist/2, outer_w/2-mirror_bolt_dist/2, h])
		rotate([0, 0, -90])
		simple_pitch_m3(h1=1, h2=0);
	translate([outer_w/2-mirror_bolt_dist/2, outer_w/2, h])
		rotate([0, 0, 180])
		simple_pitch_m3(h1=1, h2=0);
}

module MirrorBackFrame(
	h = 2,
	inner_h = 0.6,
	inner_w = 15,
	mirror_bolt_d = 3.2,
	mirror_bolt_dist = 21,
	outer_w = 29,
) {
	difference() {
		cube([outer_w, outer_w, h+inner_h]);
		translate([(outer_w-inner_w)/2, (outer_w-inner_w)/2, h]) cube([inner_w, inner_w, inner_h+1]);
		translate([outer_w/2+mirror_bolt_dist/2, outer_w/2+mirror_bolt_dist/2, -1]) cylinder(d=mirror_bolt_d, h=h+inner_h+2, $fn=20);
		translate([outer_w/2+mirror_bolt_dist/2, outer_w/2-mirror_bolt_dist/2, -1]) cylinder(d=mirror_bolt_d, h=h+inner_h+2, $fn=20);
		translate([outer_w/2-mirror_bolt_dist/2, outer_w/2, -1]) cylinder(d=mirror_bolt_d, h=h+inner_h+2, $fn=20);
	}
}

module MirrorHolder(
	rod_d = 4.8,
	rod_h = 30,
	rod_holder_d = 8.6,
	rod_holder_notch_z = 1.56,
	rod_pitch_z = 26,
	mirror_holder_th = 3,
	mirror_holder_w = 29,
	mirror_holder_z = 2.2,
	mirror_bolt_d = 3.2,
	mirror_bolt_dist = 21,
	mirror_offset = 8,// adjust to center the mirror, depending on the bolts/springs length
	mirror_th = 1,
	mirror_w = 14,
	
	debug = false,
	mirror_back_frame_h = 2,
	mirror_bolt_l = 20,
	spring_l = 9,
) {
	difference() {
		union() {
			cylinder(d=rod_holder_d, h=rod_h, $fn=60);
			translate([0, mirror_offset, rod_h+sqrt(2)*mirror_holder_th+mirror_holder_z])
				rotate([45, 0, 0])
				translate([-mirror_holder_w/2, -mirror_holder_w/2, -mirror_holder_th])
				difference() {
					cube([mirror_holder_w, mirror_holder_w, mirror_holder_th]);
					translate([mirror_holder_w/2+mirror_bolt_dist/2, mirror_holder_w/2-mirror_bolt_dist/2, 0]) cylinder(d=mirror_bolt_d, h=mirror_holder_th, $fn=20);
					translate([mirror_holder_w/2+mirror_bolt_dist/2, mirror_holder_w/2+mirror_bolt_dist/2, 0]) cylinder(d=mirror_bolt_d, h=mirror_holder_th, $fn=20);
					translate([mirror_holder_w/2-mirror_bolt_dist/2, mirror_holder_w/2, 0]) cylinder(d=mirror_bolt_d, h=mirror_holder_th, $fn=20);
				}
			translate([0, 0, rod_pitch_z-4.375])
				rotate([0, 0, 90])
				rod_pitch_m3(rod_d/2);//, t=(rod_holder_d-rod_d)/2);
		}
		cylinder(d=rod_d, h=rod_h+mirror_holder_th*2+mirror_holder_w, $fn=60);
		translate([0, 0, rod_h-rod_holder_notch_z])
			rotate([45, 0, 0])
			translate([-rod_holder_d/2, -rod_holder_d/sqrt(2)-1, 0])
			cube([rod_holder_d, sqrt(2)*rod_holder_d+2, 2*sqrt(2)*rod_holder_notch_z]);
		translate([0, 0, rod_pitch_z-4.375])
			rotate([-90, 0, 0])
			cylinder(r=1.725, h=rod_holder_d+10, $fn=20);
	}
	if(debug) {
		color("cyan")
			translate([0, mirror_offset, rod_h+sqrt(2)*mirror_holder_th+mirror_holder_z])
			rotate([45, 0, 0])
			translate([-mirror_w/2, -mirror_w/2, spring_l+mirror_back_frame_h])
			cube([mirror_w, mirror_w, mirror_th]);
		color([0.8, 0.8, 0.8, 0.5])
			translate([0, mirror_offset, rod_h+sqrt(2)*mirror_holder_th+mirror_holder_z])
			rotate([45, 0, 0])
			translate([mirror_bolt_dist/2, -mirror_bolt_dist/2, -mirror_holder_th])
			union() {
				cylinder(d=3, h=mirror_bolt_l, $fn=20);
				translate([0, 0, -2]) cylinder(d=5, h=2, $fn=20);
			}
		color([0.8, 0.8, 0.8, 0.5])
			translate([0, mirror_offset, rod_h+sqrt(2)*mirror_holder_th+mirror_holder_z])
			rotate([45, 0, 0])
			translate([mirror_bolt_dist/2, mirror_bolt_dist/2, -mirror_holder_th])
			union() {
				cylinder(d=3, h=mirror_bolt_l, $fn=20);
				translate([0, 0, -2]) cylinder(d=5, h=2, $fn=20);
			}
		color([0.8, 0.8, 0.8, 0.5])
			translate([0, mirror_offset, rod_h+sqrt(2)*mirror_holder_th+mirror_holder_z])
			rotate([45, 0, 0])
			translate([-mirror_bolt_dist/2, 0, -mirror_holder_th])
			union() {
				cylinder(d=3, h=mirror_bolt_l, $fn=20);
				translate([0, 0, -2]) cylinder(d=5, h=2, $fn=20);
			}
		translate([0, mirror_offset, rod_h+sqrt(2)*mirror_holder_th+mirror_holder_z])
			rotate([45, 0, 0])
			translate([-mirror_holder_w/2, -mirror_holder_w/2, spring_l])
			MirrorBackFrame(h=mirror_back_frame_h);
		translate([0, mirror_offset, rod_h+sqrt(2)*mirror_holder_th+mirror_holder_z])
			rotate([45, 0, 0])
			translate([-mirror_holder_w/2, -mirror_holder_w/2, spring_l+mirror_back_frame_h+mirror_th])
			MirrorFrontFrame(h=mirror_back_frame_h);
	}
}

MirrorHolder(debug=false);
//translate([-50, 0, 0]) MirrorBackFrame();
//translate([30, 0, 0]) MirrorFrontFrame();
