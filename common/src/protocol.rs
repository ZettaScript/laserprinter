use hubpack::SerializedSize;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize, SerializedSize)]
pub enum Command {
	LaserSet { power: u8 },
	LineBufferLenGet,
	LineBufferPrint { offset: u16, len: u16, x: i16 },
	LineBufferSet { offset: u16, data: [u8; 32] },
	Ping,
	// Milliseconds
	RMove { r: u16 },
	RSpeedSet { r_speed: u8 },
	// Milliseconds
	SMove { s: u16 },
	SSpeedSet { s_speed: u8 },
	// Milliseconds
	TimeGet,
	// Milliseconds
	Wait { duration: u16 },
	XGet,
	XMoveAbsolute { x: i16 },
	XMoveRelative { x: i16 },
	XMoveToOrigin,
	XSet { x: i16 },
	XSpeedSet { x_speed: u8 },
}

#[derive(Clone, Debug, Deserialize, Serialize, SerializedSize)]
pub enum Response {
	BadChecksum,
	BatchDone,
	EndstopTimeout,
	LineBufferLen { len: u16 },
	Pong,
	Time { time: u32 },
	UnknownCommand,
	X { x: i16 },
}

pub fn compute_checksum(payload: &[u8]) -> u8 {
	payload.iter().fold(0, |checksum, b| checksum ^ b)
}
