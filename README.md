# LASER Printer

This is not a boring LASER printer projecting expensive toner on the paper. This project actually uses a visible light beam to heat and burn the paper. It can be used for inkless printing or cutting.

**Warning: LASERs are dangerous.** Do not use them without appropriate protection (goggles adapted to beam's wavelength and power), even if you're only exposed to reflections. Keep them away from children or animals. Make sure not to start a fire. Always use them in a controlled direction (e.g. towards bottom and towards a matt surface) to avoid hurting/dazzling someone by accident.

**Project status**:
* Not usable in production yet.
* This is not a complete, ready-made recipe. Building this printer will require some work. Please share your experience and works if you try it!

**Motivation**:
* Sold printers are designed to stop working without reason.
* Ink and toner are very expensive.
* [Sold printers use steganography](https://en.wikipedia.org/wiki/Printer_steganography) to track whistleblowers.
* LASER!

## Hardware

### Materials needed

* **Inkjet printer**
* **Arduino** (or other AVR board)
  * Only Arduino Uno with Atmega328p and Arduino Mega2560 are supported for now. Support for other boards can be added, see [avr-hal](https://github.com/Rahix/avr-hal/).
* **L298N** (5V/12V dual motor controller)
  * You'll find assemblies with some more electronics integrated to make it easier to use (12V->5V regulator, diodes, capacitors, heatsink...).
* **500mW 405nm LASER**
  * Do not buy a LASER module from China on Ebay. You're not sure it will be shipped or function correctly. The only DIY-friendly reliable seller I've found in Europe is [Odicforce](https://odicforce.com/). (I'm not paid to write this.)
  * 500mW is good to begin with, and perfect for printing or cutting paper without cutting your table. (also, for >500mW you need more expensive safety goggles)
* **Adapted security goggles**
* **5V+12V power supply** (computer PSU is perfect)
  * Choose a good PSU. The motors won't work if the voltage goes too low below 12V.
  * Motor duty needs to be fine-tuned according to the PSU. e.g. lower duty is needed at 12.1V than at 11.5V.
* **3D printer** _(maybe optional)_
  * If your printer's carriage is not convenient for fixing the LASER module or the endstop spring, you have to make a new one. This repo provides an 3D-printable OpenSCAD model but it may not fit your printer, in that case you have to design it yourself.

You have to disassemble the inkjet printer, replace the inkjet module by the LASER module (watch out its focal length, it should be adjustable). You have to leave the following components intact and usable (or replace them):
* the carriage with the movement encoder, the belt and the position ribbon (some reverse-engineering may be needed for using the encoder)
* the carriage motor
* the paper roll motor with its movement encoder
* the paper stock motor (on some printers, this is just the paper roll motor in the other direction)

### Assembly

See the KiCad schema.

For the home position sensor (called "endstop") we use a metal spring attached to the carriage. It should touch a metallic part of the frame only when the carriage is at home position. Ensure it won't come apart if the carriage moves brutally. Home position not being detected may result in damage to the carriage motor, chassis and electronics.

## Software

### Install toolchain

Install [rustup](https://rustup.rs), then install ravedude:

```bash
cargo install ravedude
```

The needed AVR toolchain for the microcontroler will be installed automatically before compiling the program.

### Configure your board

Choose your board in [printer/.cargo/config.toml](./printer/.cargo/config.toml) (at the two places) and in the default features in [printer/Cargo.toml](./printer/Cargo.toml).

If your board is not listed, add the corresponding config from [avr-hal-template](https://github.com/Rahix/avr-hal-template). A lot of things need to be added in the Rust code, especially regarding interrupts and pin mapping.

### Burn the program

Connect the Arduino with USB port, then burn the program:

```bash
cd printer && cargo run --release
# CTRL+C to exit serial console
```

### Control from computer

```bash
cd client && cargo run --release
```

## Protocol

The high-level serial communication protocol is defined in `common`. There is no versionning in the protocol, hence you have to ensure the printer and the client are at the same version.

The low-level serial communication protocol is not yet implemented in `common`, but directly in `client` and `printer`.

Packets are very simple:
* `packet[0]`: payload length
* `packet[1]`: payload checksum (bitwise XOR of all payload bytes)
* `packet[2..2+packet[0]]`: payload (concatenation of multiple `Command` or `Response` encoded using `hubpack`)

The printer first reads all the payload, checks the checksum (abort if invalid), then executes the commands one by one.

In order to avoid breaking things or starting a fire, if you start motors or turn LASER on in a packet, you should always stop them in the same packet. A packet can always be lost and you may be too far from the big red emergency stop button.

## Alternative design

This section presents an idea for an alternative, more efficient design.

The sheet is maintained in cylindrical shape, sliding along the cylinder's axis (Y). The LASER module is static at the center of the cylinder, aligned with the Y axis. A mirror is rotating at constant speed on Y, facing the beam at 45° so it reflects at right angle and hits the paper, describing a helix when X and Y are moving.

The main advantage is the constant speed. No need to accelerate and decelerate the fragile and heavy LASER module. The print should be faster.

Challenges:
* Good 3D-printed chassis for maintaining the paper perfectly cylindrical and allowing fine tuning the beam and mirror
* Synchronize the motors
* grab the upper sheet from the paper stock and shape it to fit the cylinder

Ideas for later:
* removable blockers for allowing multiple paper sizes
* sliding the paper on a slightly helicoidal path so X can be constant speed instead of stepping

Blocking TODO:
* Determine the printing speed for a 5W 450nm LASER. Needed to know which type of motor to use, and whether gears are needed.

## License

GNU AGPL v3, CopyLeft 2022-2023 Pascal Engélibert [(why copyleft?)](https://txmn.tk/blog/why-copyleft/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.  
You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
